# Cordova-Vue Template

Base app working for browser testing and android, with the following structure:
* Base cordova project
* Source code in `src`
* vue-cli project in `src/ui` configured to compile in ES5 using babel
* vue.config.js with the configuration set to compile into the cordova assets dir `www`

```
module.exports = {
  publicPath: './',
  outputDir: '../../www',
  indexPath: './index.html',
  pages: {
    index: {
      entry: './src/main.js',
      template: '../../index.html'
    }
  }
}
```

* `src/ui/public/index.html` with the necessary metadata and `cordova.js` script:
```
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *; img-src 'self' data: content:;">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="initial-scale=1, width=device-width, viewport-fit=cover">
    <meta name="color-scheme" content="light dark">
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <title><%= htmlWebpackPlugin.options.title %></title>
  </head>
  <body>
    <div id="app"></div>
    <script src="cordova.js"></script>
    <!-- built files will be auto injected -->
  </body>
</html>
```
* Vue code inside `src/ui/src`

Once this is done, the building progress is:
```
# Inside the vue directory
$ npm run build
# Inside the cordova directory
$ cordova run browser  # to run in the browser
$ cordova run android   # to run in the device, with debug mode and usb transfer files enabled
```

The result in both browser and android should be the same as a result of rendering the Vue
components correctly.


## TODOs

* [ ] Have a node process running in background where we can have an express API serving locally
* [ ] Try hypercore tools, probably figure out the local storage permission configuration to have
    hypercore operating well and saving the storage properly.
