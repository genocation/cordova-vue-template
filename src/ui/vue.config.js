module.exports = {
  publicPath: './',
  outputDir: '../../www',
  indexPath: './index.html',
  pages: {
    index: {
      entry: './src/main.js',
      template: '../../index.html'
    }
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:5000',
        ws: true,
        changeOrigin: true
      }
    },
    overlay: {
      warnings: false,
      errors: false
    }
  }
}
